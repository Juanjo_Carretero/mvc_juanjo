package controller;

import model.dto.Cliente;
import model.dto.Videos;
import model.service.VideoServ;
import views.BuscarActualizar;
import views.BuscarActualizarVideo;
import views.Crear;
import views.CrearVideo;
import views.Eliminar;
import views.EliminarVideo;
import views.MenuPrincipal;

public class VideoController {

	private VideoServ videoServ;
	private BuscarActualizarVideo buscarActualizar;
	private CrearVideo crear;
	private EliminarVideo eliminar;
	private MenuPrincipal menuPrincipal;
	/**
	 * @return the videoServ
	 */
	public VideoServ getVideoServ() {
		return videoServ;
	}
	/**
	 * @param videoServ the videoServ to set
	 */
	public void setVideoServ(VideoServ videoServ) {
		this.videoServ = videoServ;
	}
	/**
	 * @return the buscarActualizar
	 */
	public BuscarActualizarVideo getBuscarActualizar() {
		return buscarActualizar;
	}
	/**
	 * @param buscarActualizar the buscarActualizar to set
	 */
	public void setBuscarActualizar(BuscarActualizarVideo buscarActualizar) {
		this.buscarActualizar = buscarActualizar;
	}
	/**
	 * @return the crear
	 */
	public CrearVideo getCrear() {
		return crear;
	}
	/**
	 * @param crear the crear to set
	 */
	public void setCrear(CrearVideo crear) {
		this.crear = crear;
	}
	/**
	 * @return the eliminar
	 */
	public EliminarVideo getEliminar() {
		return eliminar;
	}
	/**
	 * @param eliminar the eliminar to set
	 */
	public void setEliminar(EliminarVideo eliminar) {
		this.eliminar = eliminar;
	}
	/**
	 * @return the menuPrincipal
	 */
	public MenuPrincipal getMenuPrincipal() {
		return menuPrincipal;
	}
	/**
	 * @param menuPrincipal the menuPrincipal to set
	 */
	public void setMenuPrincipal(MenuPrincipal menuPrincipal) {
		this.menuPrincipal = menuPrincipal;
	}
	
	//Hace visible las vistas de Crear, Buscar y actualizar, Eliminar
		public void mostrarMenuPrincipal() {
			menuPrincipal.setVisible(true);
		}
		
		public void mostrarVentanaCreacion() {
			crear.setVisible(true);
		}
		public void mostrarVentanaBusActu() {
			buscarActualizar.setVisible(true);
		}
		public void mostrarVentanaEliminar() {
			eliminar.setVisible(true);
		}
		
		//Llamadas a los metodos CRUD de la capa service para validar los datos de las vistas
			public void registrarVideo(Videos miVideo) {
				videoServ.validarRegistro(miVideo);
			}
			
			public Videos buscarVideo(String codigoCliente) {
				return videoServ.validarConsulta(codigoCliente);
			}
			
			public void modificarVideo(Videos miVideo) {
				videoServ.validarModificacion(miVideo);
			}
			
			public void eliminarVideo(String codigo) {
				videoServ.validarEliminacion(codigo);
			}
}
