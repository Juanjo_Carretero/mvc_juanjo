package model.service;

import javax.swing.JOptionPane;

import controller.VideoController;
import model.dao.VideoDao;
import model.dto.Videos;

public class VideoServ {
	private VideoController videoController;
	public static boolean consultaVideo=false;
	public static boolean modificaVideo=false;
	

	public void setVideoController(VideoController videoController) {
		this.videoController = videoController;
	}
	

	public VideoController getVideoController() {
		return videoController;
	}
	
	
	//Metodo que valida los datos de Registro antes de pasar estos al DAO
	public void validarRegistro(Videos miVideo) {
		
		if(existeCliente(miVideo.getCliente_id())) {
			VideoDao miVideoDao = new VideoDao();
			miVideoDao.registrarVideo(miVideo);
		}else {
			JOptionPane.showMessageDialog(null,"No existe el cliente con id: "+miVideo.getCliente_id(),"Advertencia",JOptionPane.WARNING_MESSAGE);
		}					
	}
	
	private boolean existeCliente(int cliente_id) {
		
		ClienteServ cs = new ClienteServ();
		
		if (cs.validarConsulta(String.valueOf(cliente_id)) != null) {
			return true;
		}
		return false;
	}


	public Videos validarConsulta(String codigoVideo) {
		VideoDao miVideoDao;
		
		try {
			int codigo=Integer.parseInt(codigoVideo);	
			if (codigo > 0) {
				miVideoDao = new VideoDao();
				consultaVideo=true;			
				return miVideoDao.buscarVideo(codigo);						
			}else{
				JOptionPane.showMessageDialog(null,"El codigo del video no puede estar vacio.","Advertencia",JOptionPane.WARNING_MESSAGE);
				consultaVideo=false;
			}
			
		}catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,"Se ha presentado un Error","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		}
					
		return null;
	}
	
	//Metodo que valida los datos de Modificación antes de pasar estos al DAO
    public void validarModificacion(Videos miVideo) {
        VideoDao miVideoDao;
        if (existeCliente(miVideo.getCliente_id())) {
            miVideoDao = new VideoDao();
            miVideoDao.modificarVideo(miVideo);
            modificaVideo=true;
        }else{
            JOptionPane.showMessageDialog(null,"No existe el cliente con id: "+miVideo.getCliente_id(),"Advertencia",JOptionPane.WARNING_MESSAGE);
            modificaVideo=false;
        }
    }
	
	//Metodo que valida los datos de Eliminación antes de pasar estos al DAO
	public void validarEliminacion(String codigoVideo) {
		VideoDao miVideoDao=new VideoDao();
		try {
			int codigo=Integer.parseInt(codigoVideo);
			miVideoDao.eliminarVideo(codigo);
		}catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(null,"Debe ingresar un dato numerico","Error",JOptionPane.ERROR_MESSAGE);
			consultaVideo=false;
		}
		
	}

}
